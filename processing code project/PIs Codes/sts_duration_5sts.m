function duration_5sts = sts_duration_5sts(chair_data)

% duration_5sts = sts_duration_5sts(chair_data)
%
% PI2: 5sts duration - this function calculates the PI1: 5STS duration - it is calculated as
% the time elapsed between the first movement after the GO signal and the 
% fifth dynamic contact with the chair (thus excluding the initial static 
% contact). Only the data coming from the force plates are used for 
% calculating this PI.
%
%INPUT:
%
%
%OUTPUT:
%
%

