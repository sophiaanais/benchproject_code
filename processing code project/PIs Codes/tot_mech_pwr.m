function CoM_work = tot_mech_pwr(chair_data,imu_data)

%CoM_work = tot_mech_pwr(chair_data,imu_data)
%
% PI7: total mechanical power � this PI consists of a scalar indicating the 
% total mechanical work done by the Center of mass. The CoM work is calculated
% as the scalar product between the CoM velocity and the force plates resultant
% force
%
%INPUT:
%
%
%OUTPUT:
%
%