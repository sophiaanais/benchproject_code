function ncycles = repetitions_30sSTS(chair_data, imu_data)

%ncycles = repetitions_30sSTS(chair_data, imu_data)
%
% PI1: 30sSTS duration - it is calculated as the number of full sit-to-stand 
% cycles executed in the 30s after the GO signal. Only the data coming from 
% the force plates are used for calculating this PI.
%
%INPUT:
%
%
%OUTPUT:
%
%