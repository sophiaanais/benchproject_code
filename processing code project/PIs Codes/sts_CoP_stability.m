function [CoP_dist_AP, CoP_dist_ML] = sts_CoP_stability(chair_data, imu_data)

% [CoP_dist_AP, CoP_dist_ML] = sts_CoP_stability(chair_data, imu_data)
%
% PI3: STS CoP stability � this PI consists of a 2 elements array of scalars 
% indicating the average distance travelled by the CoP both in AP and ML directions. 
% Distance data are averaged across the 5 STS cycles. 
% Data from both the Chair and lower limb kinematics are needed for calculating this PI. 
%
%INPUT:
%
%
%OUTPUT:
%
%

